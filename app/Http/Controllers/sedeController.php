<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatesedeRequest;
use App\Http\Requests\UpdatesedeRequest;
use App\Repositories\sedeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class sedeController extends AppBaseController
{
    /** @var  sedeRepository */
    private $sedeRepository;

    public function __construct(sedeRepository $sedeRepo)
    {
        $this->sedeRepository = $sedeRepo;
    }

    /**
     * Display a listing of the sede.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sedeRepository->pushCriteria(new RequestCriteria($request));
        $sedes = $this->sedeRepository->all();

        return view('sedes.index')
            ->with('sedes', $sedes);
    }

    /**
     * Show the form for creating a new sede.
     *
     * @return Response
     */
    public function create()
    {
        return view('sedes.create');
    }

    /**
     * Store a newly created sede in storage.
     *
     * @param CreatesedeRequest $request
     *
     * @return Response
     */
    public function store(CreatesedeRequest $request)
    {
        $input = $request->all();

        $sede = $this->sedeRepository->create($input);

        Flash::success('Sede saved successfully.');

        return redirect(route('sedes.index'));
    }

    /**
     * Display the specified sede.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Flash::error('Sede not found');

            return redirect(route('sedes.index'));
        }

        return view('sedes.show')->with('sede', $sede);
    }

    /**
     * Show the form for editing the specified sede.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Flash::error('Sede not found');

            return redirect(route('sedes.index'));
        }

        return view('sedes.edit')->with('sede', $sede);
    }

    /**
     * Update the specified sede in storage.
     *
     * @param  int              $id
     * @param UpdatesedeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesedeRequest $request)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Flash::error('Sede not found');

            return redirect(route('sedes.index'));
        }

        $sede = $this->sedeRepository->update($request->all(), $id);

        Flash::success('Sede updated successfully.');

        return redirect(route('sedes.index'));
    }

    /**
     * Remove the specified sede from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Flash::error('Sede not found');

            return redirect(route('sedes.index'));
        }

        $this->sedeRepository->delete($id);

        Flash::success('Sede deleted successfully.');

        return redirect(route('sedes.index'));
    }
}
