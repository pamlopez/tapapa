<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class emailController extends Controller
{
    //pendiente: funcion para mostrar formulario de contacto

   //Prueba de correo

    public function test(Request $request){
        //Logic will go here
        $nombre="hola";
        $email="selmyfulgan@gmail.com";
        $mensaje="Mensaje de prueba, enviado desde laravel";

        \Mail::send('emails.contacto',['nombre'=>$nombre, 'email'=>$email,'mensaje'=>$mensaje], function($message) use ($email) {
            $message->from(env('MAIL_FROM'),env('MAIL_NAME'));
            $message->to(env('MAIL_TO'));
            $message->cc($email);
            $message->subject("Mensaje de prueba desde GGM");
        });
        return "Exito, correo enviado a $email, desde ".env('MAIL_FROM');
    }



    //Enviar el correo en base a los datos del formulario de contacto
    public function send(Request $request){
        //Logic will go here
        $nombre=$request->nombre;
        $email=$request->email;
        $mensaje=$request->mensaje;

        \Mail::send('emails.contacto',['nombre'=>$nombre, 'email'=>$email,'mensaje'=>$mensaje], function($message) use ($email) {
            $message->from(env('MAIL_FROM'),env('MAIL_NAME'));
            $message->to(env('MAIL_TO'));
            $message->cc($email);
            $message->subject("Mensaje desde el portal de Pa'donde");
        });
        return redirect('contacto')
            ->with(['message' => $email]);
    }

}
