<?php

namespace App\Repositories;

use App\Models\cat_item;
use InfyOm\Generator\Common\BaseRepository;

class cat_itemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cat',
        'descripcion',
        'orden',
        'otro',
        'predeterminado'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cat_item::class;
    }
}
