<?php

namespace App\Repositories;

use App\Models\cat_cat;
use InfyOm\Generator\Common\BaseRepository;

class cat_catRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'usuario_ingreso',
        'ip_ingreso',
        'fh_ingreso',
        'usuario_ultima_mod',
        'ip_ultima_mod',
        'fh_ultima_mod'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cat_cat::class;
    }
}
