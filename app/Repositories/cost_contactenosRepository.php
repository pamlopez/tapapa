<?php

namespace App\Repositories;

use App\Models\cost_contactenos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_contactenosRepository
 * @package App\Repositories
 * @version March 8, 2019, 6:32 am UTC
 *
 * @method cost_contactenos findWithoutFail($id, $columns = ['*'])
 * @method cost_contactenos find($id, $columns = ['*'])
 * @method cost_contactenos first($columns = ['*'])
*/
class cost_contactenosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'telefono',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_contactenos::class;
    }
}
