<?php

namespace App\Repositories;

use App\Models\persona;
use InfyOm\Generator\Common\BaseRepository;

class personaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'n1',
        'n2',
        'a1',
        'a2',
        'id_tipo_documento_identificacion',
        'no_identificacion',
        'genero',
        'id_depto_domicilio',
        'id_muni_domicilio',
        'zona_domicilio',
        'direccion_domicilio',
        'id_etnia'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return persona::class;
    }
}
