<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class users_rol extends Model
{


    // use SoftDeletes;

    public $table = 'users_rol';
    protected $primaryKey='id_users_rol';
    public $timestamps=false;

    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';
    //protected $dates = ['deleted_at'];

    public $fillable = [
        'id_users_rol',
        'descripcion',
        'otro',
    ];

    public static function listado_items($vacio='') {
        $listado=users_rol::
        orderby('descripcion','asc')
            ->pluck('descripcion','id_users_rol');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;
    }
}
