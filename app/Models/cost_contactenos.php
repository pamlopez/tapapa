<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_contactenos
 * @package App\Models
 * @version March 8, 2019, 6:32 am UTC
 *
 * @property string email
 * @property string telefono
 * @property string descripcion
 */
class cost_contactenos extends Model
{
   // use SoftDeletes;

    public $table = 'cost_contactenos';
    public $timestamps=false;
   // const CREATED_AT = 'created_at';
   // const UPDATED_AT = 'updated_at';


   // protected $dates = ['deleted_at'];


    public $fillable = [
        'email',
        'telefono',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_cost_contactenos' => 'integer',
        'email' => 'string',
        'telefono' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


 public function getIdAttribute() {
        return $this->id_cost_contactenos;
    }
    
}
