<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class administracion extends Model
{
    public static function rol_asignado($id_user)
    {
        $query = users_asignacion::join('users_rol', 'users_rol.id_users_rol', '=', 'users_asignacion.id_rol')
            ->where('id_users', $id_user)->get();
        return $query;

    }

    public function rel_id_sede()
    {
        return $this->hasMany(\App\Models\sede::class, 'id_sede', Auth::user()->id_sede);
    }

    public function getFmtCaimuAttribute()
    {
        if ($this->rel_id_caimu()) {
            return $this->rel_id_sede()->descripcion;
        } else {
            return "-";
        }
    }

    public static function sede()
    {
        return $query = sede::where('id_sede', Auth::user()->id_caimu)->get()->first();
    }

    public static function seleccionados_roles($id)
    {
        $listado = users_asignacion::select(DB::raw('id_rol'))
            ->where('id_users', $id)->get();
        $list = [];


        if (!empty($listado)) {
            foreach ($listado as $item) {

                $list[] = intval($item->id_rol);
            }
        } else {
            $list = [];
        }

        // $list = implode(',',$list);


        return $list;
    }


}
