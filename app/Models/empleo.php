<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class empleo
 * @package App\Models
 * @version October 16, 2018, 8:09 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property string puesto
 * @property string link
 * @property string descripcion
 * @property string otros
 */
class empleo extends Model
{
   // use SoftDeletes;

    public $table = 'empleo';
    
 //   const CREATED_AT = 'created_at';
 //   const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];
    protected $primaryKey='id_empleo';
    public $timestamps=false;

    public $fillable = [
        'puesto',
        'link',
        'descripcion',
        'otros'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'puesto' => 'string',
        'link' => 'string',
        'descripcion' => 'string',
        'otros' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getIdAttribute() {
        return $this->id_empleo;
    }
}
