<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class caimus extends Model
{
    public $table = 'caimus';
    protected $primaryKey='id_caimus';
    public $timestamps=false;

    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';
    //protected $dates = ['deleted_at'];


    public $fillable = [
        'id_caimus',
        'descripcion',
    ];

    public static function listado_caimus($vacio=''){
        $listado= caimus::orderBy('id_caimus','asc')
            ->pluck('descripcion','id_caimus');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;

    }

}
