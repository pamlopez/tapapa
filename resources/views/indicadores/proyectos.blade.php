@extends('layouts.app')
@section('content_header')
    <style>
        .totales{
            color:#ffffff;
        }
    </style>
    {{-- KPI --}}
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><b><i class="fa fa-line-chart" aria-hidden="true"></i> Indicadores</b></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="small-box bg-red-active">
                        <div class="inner">
                            <h3 class="totales">{{ $total_historico->total}}</h3>
                            <p class="totales">Total hist&oacute;rico de proyectos </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-building-o" aria-hidden="true"></i>
                        </div>
                        <p class="small-box-footer">Proyectos en total</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="small-box bg-blue-active">
                        <div class="inner">
                            <h3 class="totales">{{ $total_proyecto_anio->total}}</h3>
                            <p class="totales">Proyectos ingresados</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-home"></i>
                        </div>
                        <p class="small-box-footer">Proyectos ingresados en el año en curso</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="small-box bg-green">
                        <div class="inner">

                                @foreach($total_sectores_afectados as $r)
                                    @php($subtot[]=(int)$r->total)
                                @endforeach
                            <h3 class="totales">{{array_sum($subtot)}}</h3>

                            <p class="totales">Total de Sectores </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-briefcase"></i>
                        </div>
                        <p class="small-box-footer">Revisados Históricamente</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection

@section('content')
    <style>
        .chart { padding: 15px;}
    </style>
    <div class="clearfix"></div>
    @include('flash::message')
    <div class="clearfix"></div>

    <div>
    {{-- Formulario de personalización --}}
        @include('partials.control_fechas_simple')
        {{-- Asesorías --}}
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><b> <i class="fa fa-bar-chart" aria-hidden="true"></i> Desglose de indicadores </b></h3>
                    </div>
                    <div class="box-body">
                        <!--ESTADOS DE PROYECTO -->
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div id="g_mes_a" class="chart">
                                    Clasificaci&oacute;n de estado actual del proyecto
                                </div>
                            </div>
                            <div class="col-sm-6">
                                Desglose de la informaci&oacute;n
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-striped ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Estados de proyeto</th>
                                            <th>Proyectos</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($tabla_estado_actual_proyecto  as $id => $valor)
                                            <tr>
                                                <td> {{ $i++ }}</td>

                                                <td>
                                                    {{ $valor->text }}
                                                </td>
                                                <td>
                                                    {{ $valor->identificador }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <!--entidad adquisicion -->
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div id="g_entidad" class="chart">
                                    entidad adquisicion
                                </div>
                            </div>
                            <div class="col-sm-6">
                                Desglose de la informaci&oacute;n
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-striped ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Entidad</th>
                                            <th>Cantidad</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($total_entidad_adquisicion  as $id => $valor)
                                            <tr>
                                                <td> {{ $i++ }}</td>

                                                <td>
                                                    {{ $valor->txt }}
                                                </td>
                                                <td>
                                                    {{ $valor->valor }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--SECTORES AFECTADOS -->
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div id="g_sectores" class="chart">
                                    Sectores
                                </div>
                            </div>
                            <div class="col-sm-6">
                                Desglose de la informaci&oacute;n
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-striped ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Sectores</th>
                                            <th>Cantidad</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($tabla_sectores_afectados_detalle  as $id => $valor)
                                            <tr>
                                                <td> {{ $i++ }}</td>

                                                <td>
                                                    {{ $valor->txt }}
                                                </td>
                                                <td>
                                                    {{ $valor->valor }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection
@include('indicadores._script_dash')
