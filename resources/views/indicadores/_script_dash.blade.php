@push("javascript")
    <script>
        $(function () {
            $('#g_entidad').highcharts(
                    {!! $datitos->g_entidad_adquisicion!!}
            )
        });

        $(function () {
            $('#g_sectores').highcharts(
                    {!! $datitos->g_sectores!!}
            )
        });
        //Primera fila
        $(function () {
            $('#g_mes_a').highcharts(
                    {!! $datitos->g_estado_actual_proyecto!!}
            )
        });

    </script>

@endpush
