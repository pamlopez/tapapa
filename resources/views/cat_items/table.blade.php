<div class="table-responsive">
    <table class="table table-striped table-bordered" id="catItemstable">
        <thead style="text-align: center">
            <th>#</th>
            <th>Catálogo</th>
            <th>Descripción</th>
            <th colspan="3">Acciones</th>
        </thead>
        <tbody>
        @php($cnt =1)
        @foreach($listado as $id => $catItem)
            @php($clase = $catItem->otro == 1?'success':'primary' )
            <tr>
                <td>{{  (($listado->currentPage() -1) * $listado->perPage())+1+$id }}</td>
                <td>{!! $catItem->fmt_id_cat !!}</td>
                <td>{!! $catItem->descripcion !!}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{!! route('catItems.edit', [$catItem->id]) !!}" class='btn btn-warning btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        <button type="button" class="btn btn-{{$clase}} btn-xs" data-toggle="modal" data-target="#modal-warning{{$catItem->id}}">
                            @if($catItem->otro == 1)
                                Habilitar
                            @else
                                Deshabilitar
                            @endif
                        </button>
                        <div class="modal modal-{{$clase}} fade" id="modal-warning{{$catItem->id}}" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title"><i class="fa fa-window-close" aria-hidden="true"></i>
                                            @if($catItem->otro == 1)
                                                Habilitar registro:
                                            @else
                                                Deshabilitar registro:
                                            @endif
                                        </h4>
                                    </div>
                                    {!! Form::open(['url' => ['itemdeshabilitado', $catItem->id], 'method' => 'get']) !!}
                                    <div class="modal-body">
                                        <h3 align="center"><b>{!! $catItem->descripcion !!}</b></h3>
                                        <br>
                                        @if($catItem->otro == 1)
                                            <p align="left"><h4>Esta opción la verá reflejada en los catálogos correspondientes de la aplicación.</h4></p>
                                            {!! Form::hidden('otro',null, ['class' => 'form-control']) !!}
                                        @else
                                            <p align="left"><h4>Esta opción no se verá reflejada en los catálogos correspondientes de la aplicación.</h4></p>
                                            {!! Form::hidden('otro',1, ['class' => 'form-control']) !!}
                                        @endif

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline">Guardar</button>
                                    </div>
                                    {!! Form::close() !!}

                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
