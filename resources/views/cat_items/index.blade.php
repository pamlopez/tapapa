@extends('layouts.app')
@section('content')
    <div class="content" style="margin-top: -3%">
        <div class="box box-purple">
            <section class="content-header">
                <legend><i class="fa fa-cubes" aria-hidden="true"></i> Gestión de catálogos</legend>
            </section>
            <div class="box-header">
                <div class="col-md-6">
                    {!! Form::open( ['url' => route('catItems.index'), 'method' => 'get']) !!}
                    <div class="form-group">
                        {!! Form::label('id_cat', 'Favor de escoger catálogo a editar:') !!}
                        {!! Form::select('id_cat', \App\Models\cat_item::listado_catalogos(), $id_cat,['class' => 'form-control', 'onChange'=>'submit()','id'=>'catalogo']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-12">
                    <h3 class="col-md-5 col-md-offset-4"><b> {{ $nombre_catalogo }} <br><small>Opciones disponbiles</small></b></h3>
                    <a href="#" class='btn btn-primary btn-sm col-md-4 col-md-offset-4' onclick="agregar()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar categor&iacute;a a {{ $nombre_catalogo }}</a>
                </div>
            </div>
            <div class="box-body">
                @include('cat_items.table')
            </div>
            <div class="box-footer">
                {!! $listado->appends(request()->except('page'))->links() !!}
            </div>
        </div>
    </div>


    {{-- Formulario MODAL para agregar un catalogo--}}

    <div class="modal fade" tabindex="-1" role="dialog" id="modal_agregar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'catItems.store']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Agregar nueva opción <small>{{ $nombre_catalogo }}</small></h3>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="id_cat" id="id_cat" value="{{ $id_cat }}">
                    <!-- Descripcion Field -->
                    <div class="form-group ">
                        {!! Form::label('descripcion', 'Descripcion:') !!}
                        {!! Form::text('descripcion', null, ['class' => 'form-control', 'required'=>'required']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Agregar nueva opción</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    {{-- Formulario MODAL para editar un catalogo--}}

    <div class="modal fade" tabindex="-1" role="dialog" id="modal_editar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open( ['route' => ['catItems.update', 0], 'method' => 'patch', 'id'=>'frm_edita']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Modificar opción existente<small> {{ $nombre_catalogo }}</small></h3>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="id_item" id="id_item" value="0">
                    <!-- Descripcion Field -->
                    <div class="form-group ">
                        {!! Form::label('descripcion', 'Descripcion:') !!}
                        {!! Form::text('descripcion', null, ['class' => 'form-control', 'required'=>'required']) !!}
                    </div>

                    <!-- Codigo Field -->
                    <div class="form-group ">
                        {!! Form::label('codigo', 'Codigo:') !!}
                        {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Orden Field -->
                    <div class="form-group ">
                        {!! Form::label('orden', 'Orden:') !!}
                        {!! Form::number('orden', 0, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection


@push("javascript")
    <script type="text/javascript">
        function editar(id) {
            $('#id_item').val(id);

            $.getJSON( '{{ action('cat_itemController@json') }}',{ "id_item" : id})
                .done(function( data ) {
                        $("#frm_edita #descripcion").val(data.descripcion);
                        $("#frm_edita #codigo").val(data.codigo);
                        $("#frm_edita #orden").val(data.orden);
                        $('#modal_editar').modal({
                            keyboard: false
                        })
                    }
                );
        }
        function agregar() {
            $('#modal_agregar').modal({
                keyboard: false
            })
        }
        $('#catalogo').select2({
            placeholder: 'Select an option'
        });

    </script>
@endpush
