<script>

    //var avanzar=1;

    function display() {
        // later record end time
        var endTime = new Date();
        // time difference in ms
        var timeDiff = endTime - startTime;
        // strip the miliseconds
        timeDiff /= 1000;
        // get seconds
        var seconds = Math.round(timeDiff % 60);
        // remove seconds from the date
        timeDiff = Math.floor(timeDiff / 60);
        // get minutes
        var minutes = Math.round(timeDiff % 60);
        // remove minutes from the date
        timeDiff = Math.floor(timeDiff / 60);
        // get hours
        var hours = Math.round(timeDiff % 24);
        // remove hours from the date
        timeDiff = Math.floor(timeDiff / 24);
        // the rest of timeDiff is number of days
        var days = timeDiff;
        $("#time").text( hours + ":" + minutes + ":" + seconds);
        if(avanzar===1) {
            setTimeout(display, 1000);
        }

    }
    //Arrancar
    function iniciar() {
        avanzar=1;
        startTime = new Date();
        setTimeout(display, 1000);
    }

    function detener() {
        avanzar=0;
    }

</script>