@extends('layouts.app')

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-default">
               <div class="box-header with-border">
                   <h3 class="box-title"><b> 1. IDENTIFICACIÓN DEL PROYECTO</b> </h3>
                   <br>
                   <h3 class="box-title">&nbsp;&nbsp;&nbsp; Nombre del proyecto :<b> {{$costProyecto->nombre_proyecto}}</b></h3>

               </div>
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costProyecto, ['route' => ['costProyectos.update', $costProyecto->id], 'method' => 'patch']) !!}

                        @include('cost_proyectos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection