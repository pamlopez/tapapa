@extends('layouts.app')
@section('content')
    <div class="content">
        <a href="{!!route('costProyectos.show',$a);!!}" class="btn btn-info"> <i class="fa fa-long-arrow-left"
                                                                                 aria-hidden="true"></i> Volver</a>
        <div class="panel">
            <div class="panel-heading">
                <h2><b style="color: #005E95"> <i class="fa fa-cloud-upload" aria-hidden="true"></i> Subir fotografías
                        al proyecto:</b>
                    <b>{{$datos_proyecto->nombre_proyecto}}</b></h2>
            </div>
            <div class="panel-body">
                <div class="col-sm-4   col-sm-offset-3">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong>Problemas al subir tu archivo.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div align="center" class="thumbnail col-sm-offset-2">
                        <h4 align="">
                            <b>Elija la fotografía que desea agregar al proyecto</b>
                        </h4>
                        <form action="{{ url('image_upload_save')}}" enctype="multipart/form-data" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_proyecto" value="{{$a}}"/>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" name="image"/>
                                </div>
                                <div class="col-md-12">
                                    <br>
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-upload" aria-hidden="true"></i> Subir foto
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                @include('cost_proyectos._listado_resultado_upload', compact('contenido'))
            </div>
@endsection