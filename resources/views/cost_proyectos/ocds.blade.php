<div class="alert alert-danger alert-dismissible">
    <h4><i class="icon fa fa-warning"></i> Advertencia!</h4>
    Esta página se encuentra en construcción. Los datos públicados pueden variar en base a su fuente.
</div>
<div class="btn-group open">
    <button type="button" class="btn btn-default">OC4IDS</button>
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{asset('costProyectos/proyecto/'.$costProyecto->id_proyecto)}}">Release 1</a></li>

    </ul>
</div>
<div class="table table-responsive">
    <pre>  <code>{{$json_format}}</code> </pre>
</div>