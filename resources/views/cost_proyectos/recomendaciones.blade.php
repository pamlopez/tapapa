<div class="row">
    <div id="exTab2" class="container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#1" data-toggle="tab">Preparación del Proyecto</a>
            </li>
            <li><a href="#2" data-toggle="tab">Acuerdos de Financiamento</a>
            </li>
            <li><a href="#3" data-toggle="tab">Información del contrato</a>
            </li>
            <li><a href="#4" data-toggle="tab">Observaciones y Recomendaciones</a>
            </li>
        </ul>

        <div class="tab-content ">
            <div class="tab-pane active" id="1">
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <h5><b> Resolución del MARN No: </b></h5>
                        <h4>{!! $costProyecto->rel_dis_plan->resolucion_marn !!}</h4>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h5><b> De fecha: </b></h5>
                        <h4>{!!\Carbon\Carbon::parse(  $costProyecto->rel_dis_plan->f_resolucion_marn )->format('d/m/Y')!!}</h4>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h5><b> Medidas de mitigación incluidas en presupuesto: </b></h5>
                        <h4>@if(strlen($costProyecto->rel_dis_plan->medida_mitigacion_presupuesto)>0)
                                Si : {!! $costProyecto->detalle_responsable !!}
                            @else
                                No
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <img src="{{asset('images/icono_suelos.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_topografia.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_geologico.png') }} " alt="">
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <h3><b>Estudio de Suelos</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->estudio_suelo_info}}"
                           data-placement="right">INFO</a>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Topografía</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->estudio_topografia_info}}"
                           data-placement="right">INFO</a>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Geológico</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->estudio_geologico_info}}"
                           data-placement="right">INFO</a>
                    </div>

                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <img src="{{asset('images/icono_especificaciones.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_memorias.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_planos.png') }} " alt="">
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <h3><b>Especificaciones Técnicas</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->espe_tecnica_info}}"
                           data-placement="right">INFO</a>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Memorias de Cálculo</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->memoria_calculo_info}}"
                           data-placement="right">INFO</a>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Planos</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->plano_completo_info}}"
                           data-placement="right">INFO</a>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <img src="{{asset('images/icono-especiales.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono-estructura.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono-generales.png') }} " alt="">
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <h3><b>Disposiciones Especiales</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->dispo_especial_info}}"
                           data-placement="right">INFO</a>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Estructuras</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->diseno_estructural_info}}"
                           data-placement="right">INFO</a>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Especificaciones Generales</b></h3>
                        <a href="#" data-toggle="tooltip"
                           title="{{$costProyecto->rel_dis_plan->espe_general_info}}"
                           data-placement="right">INFO</a>
                    </div>

                </div>
                <br>
                <br>
                <br>
                <br>


            </div>
            <div class="tab-pane" id="2">
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-12">
                        <h2><b>Estudio o Diseño</b></h2>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3><b>Partida Presupuestaria</b></h3>
                    </div>
                    <div class="col-md-6 ">
                        <h3><b>Monto Asignado</b></h3>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3>{{$costProyecto->rel_dis_plan->fuente_financiamiento_dis}}</h3>
                    </div>
                    <div class="col-md-6 ">
                        <h6>
                            Q.{{number_format ( ($costProyecto->rel_dis_plan->dis_precio_contrato) , $decimals = 2 , $dec_point = "." , $thousands_sep = "," )}}</h6>


                    </div>
                </div>

                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-12">
                        <h2><b>Supervisión</b></h2>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3><b>Partida Presupuestaria</b></h3>
                    </div>
                    <div class="col-md-6 ">
                        <h3><b>Monto Asignado</b></h3>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3>{{$costProyecto->rel_dis_plan->fuente_f_sup}}</h3>
                    </div>
                    <div class="col-md-6 ">
                        <h6>
                            Q.{{number_format ( ($costProyecto->rel_supervision->sup_monto_contratado) , $decimals = 2 , $dec_point = "." , $thousands_sep = "," )}}</h6>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-12">
                        <h2><b>Ejecución de Obra</b></h2>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3><b>Partida Presupuestaria</b></h3>
                    </div>
                    <div class="col-md-6 ">
                        <h3><b>Monto Asignado</b></h3>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3>{{$costProyecto->rel_dis_plan->fuente_f_ejecucion}}</h3>
                    </div>
                    <div class="col-md-6 ">
                        <h6>
                            Q.{{number_format ( ($costProyecto->rel_planifica->monto_contrato) , $decimals = 2 , $dec_point = "." , $thousands_sep = "," )}}</h6>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-12">
                        <h2><b>Medidas de Mitigación Ambiental</b></h2>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3><b>Partida Presupuestaria</b></h3>
                    </div>
                    <div class="col-md-6 ">
                        <h3><b>Monto Asignado</b></h3>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-6">
                        <h3>{{$costProyecto->rel_dis_plan->fuente_f_ambiental}}</h3>
                    </div>
                    <div class="col-md-6 ">
                        <h6>
                            Q.{{number_format ( ($costProyecto->rel_dis_plan->monto_ambiental) , $decimals = 2 , $dec_point = "." , $thousands_sep = "," )}}</h6>
                    </div>
                </div>

            </div>
            <div class="tab-pane" id="3">

                <div class="row" align="center">
                    <div class="col-md-3">
                        <h3><b>Empresa constructora:</b></h3>
                    </div>
                    <div class="col-md-3">
                        <h3><b>Plazo del contrato (meses):</b></h3>
                    </div>
                    <div class="col-md-3">
                        <h3><b>Fecha de Inicio:</b></h3>
                    </div>
                    <div class="col-md-3">
                        <h3><b>Tipo y No. de Contrato:</b></h3>
                    </div>
                </div>
                <div class="row" align="center">
                    <div class="col-md-3">
                        <h4>{{$costProyecto->rel_planifica->empresa_constructora}}</h4>
                    </div>
                    <div class="col-md-3">
                        <h4>{{$costProyecto->rel_planifica->plazo_contrato_m}}</h4>
                    </div>
                    <div class="col-md-3">
                        <h4>{{\Carbon\Carbon::parse( $costProyecto->rel_planifica->f_inicio_contrato)->format('d/m/Y')}}</h4>
                    </div>
                    <div class="col-md-3">
                        <h4>{{$costProyecto->rel_planifica->tipo_no_contrato}}</h4>
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-12">
                        <h2><b>Alcance del contrato:</b></h2>
                    </div>
                </div>
                <div class="row" align="center ">
                    <div class="col-md-12">
                        <h3>{{$costProyecto->rel_planifica->alcance_contrato}}</h3>
                    </div>
                </div>
                <div class="row" align="center">
                    <div class="col-md-6">
                        <h3><b>Precio contrato:</b></h3>
                    </div>
                    <div class="col-md-6">
                        <h3><b>Fecha:</b></h3>
                    </div>
                </div>
                <div class="row" align="center ">
                    <div class="col-md-6">
                        <h4>
                            Q.{{number_format ( ($costProyecto->rel_planifica->monto_contrato) , $decimals = 2 , $dec_point = "." , $thousands_sep = "," )}}</h4>
                    </div>
                    <div class="col-md-6">
                        <h4>{{\Carbon\Carbon::parse( $costProyecto->rel_planifica->f_inicio_contrato)->format('d/m/Y')}}</h4>
                    </div>
                </div>
                <div class="row" align="center">
                    <div class="col-md-6">
                        <h3><b>Aprobación contrato:</b></h3>
                    </div>
                    <div class="col-md-6">
                        <h3><b>Fecha aprobación contrato:</b></h3>
                    </div>
                </div>
                <div class="row" align="center ">
                    <div class="col-md-6">
                        <h4></h4>
                    </div>
                    <div class="col-md-6">
                        <h4>{{\Carbon\Carbon::parse( $costProyecto->rel_planifica->f_aprobacion_contrato)->format('d/m/Y')}}</h4>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="4">
                <div class="row row-bordered">
                    <div class="col-md-12 text-bottom">
                        <h2><img src="{{asset('images/icono-irregularidades.png') }} " alt="" height="40px"
                                 width="40px"> Observaciones</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1" align="justify">
                        {{$costProyecto->rel_supervision->observaciones}}
                    </div>
                </div>
                <br>
                <br>
                <div class="row row-bordered">
                    <div class="col-md-12 text-bottom">
                        <h2><img src="{{asset('images/conclusiones.jpg') }} " alt="" height="40px"
                                 width="40px"> Conclusiones</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1" align="justify">
                        {{$costProyecto->rel_supervision->conclusiones}}
                    </div>
                </div>
                <br>
                <br>
                <div class="row row-bordered">
                    <div class="col-md-12 text-bottom">
                        <h2><img src="{{asset('images/recomendaciones.png') }} " alt="" height="40px"
                                 width="40px"> Recomendaciones</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1" align="justify">
                        {{$costProyecto->rel_supervision->recomendaciones}}
                    </div>
                </div>
                <br>
                <br>
                <div class="row row-bordered">
                    <div class="col-md-12 text-bottom">
                        <h2><img src="{{asset('images/icono-noticias.png') }} " alt="" height="40px"
                                 width="40px">
                            Artículos de Interes</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>