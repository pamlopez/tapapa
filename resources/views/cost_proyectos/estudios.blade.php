<div class="row">

                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <img src="{{asset('images/icono_suelos.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_topografia.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_geologico.png') }} " alt="">
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <h3><b>Estudio de Suelos</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            INFO
                        </button>

                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Topografía</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-topo">
                            INFO
                        </button>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Geológico</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-geo">
                            INFO
                        </button>
                    </div>

                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <img src="{{asset('images/icono_especificaciones.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_memorias.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono_planos.png') }} " alt="">
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <h3><b>Especificaciones Técnicas</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-espe">
                            INFO
                        </button>

                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Memorias de Cálculo</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-memo">
                            INFO
                        </button>

                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Planos</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-plano">
                            INFO
                        </button>

                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <img src="{{asset('images/icono-especiales.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono-estructura.png') }} " alt="">
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <img src="{{asset('images/icono-generales.png') }} " alt="">
                    </div>
                </div>
                <div class="row" align="center" style="padding: 5px 15px;">
                    <div class="col-md-3">
                        <h3><b>Disposiciones Especiales</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-dispo">
                            INFO
                        </button>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Estructuras</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-estructura">
                            INFO
                        </button>

                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3><b>Especificaciones Generales</b></h3>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-general">
                            INFO
                        </button>
                    </div>

                </div>
                <br>

</div>