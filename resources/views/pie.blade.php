<footer class="main-footer">
    <div class="pull-right hidden-xs">
        @if(Auth::check())
            Usuario: <a href="{{ url("usuario") }}">{{ Auth::user()->email }}</a>
        @else
            <a href="{{ url("login") }}">Usuario no autenticado</a>

        @endif
    </div>
    <a href="http://www.energuate.com/" target="_blank">
        <img src="{{ asset('energuate.png') }}" style="height: 50px">
    </a>
</footer>
