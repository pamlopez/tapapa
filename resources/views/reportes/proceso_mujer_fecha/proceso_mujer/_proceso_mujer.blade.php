@php($titulo_reporte = "Procesos por mujer")
@php($subtitulo = "Detalle de Procesos por mujer")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> {{ $subtitulo }}</b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_agenda_responsable" class="table table-hover table-bordered ">
                        <thead>
                        <tr class="tbl-encabezados">
                            <th>Nombre</th>
                            @foreach( $consulta_proceso_mujer->groupBy("descripcion") as $kaa => $vaa)
                                <th>{{ $kaa }}</th>
                            @endforeach
                            <th>Cantidad</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @php($total_iniciado=null)
                        @php($total_piniciar=null)
                        @foreach($consulta_proceso_mujer->groupBy("nombre") as $kam =>$vam)
                            <tr>
                                <td>{{ $data_x[]=$kam }}</td>
                                @php($subtotal=[])
                                @foreach( $consulta_proceso_mujer->groupBy("descripcion") as $kaa => $vaa)
                                    @php($consulta = $vaa->where("nombre",$kam)->where("descripcion",$kaa))
                                    <td>{{ $subtotal[] = $consulta->sum("cantidad") }}</td>
                                @endforeach
                                <td>{{ $data_y[]=array_sum($subtotal)  }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @foreach( $consulta_proceso_mujer->groupBy("descripcion") as $kaa => $vaa)
                                @php($consulta = $vaa->where("descripcion",$kaa))
                                <td>{{ $consulta->sum("cantidad") }}</td>
                            @endforeach
                            <td>{{ array_sum($data_y)}}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

@push('javascript')
    <script type="text/javascript">
        var base64_data_table = '{!! $json_data_table !!}';
        var json_data_table_special = JSON.parse(atob(base64_data_table));
        json_data_table_special.searching = true;
        json_data_table_special.paging = true;
        json_data_table_special.ordering = true;
        json_data_table_special.info = true;
        json_data_table_special.scrollY=true;
        json_data_table_special.scrollX=true;
        json_data_table_special.scrollCollapse= true;
        json_data_table_special.buttons.push({"extend":"colvis","className":"btn bg-black",columnText: function ( dt, idx, title ) {
                return (idx+1)+': '+title;
            }});
        json_data_table_special.fixedColumns=   {
            leftColumns: 1
        };

        json_data_table_special.language.buttons.colvis = '<i class="fa fa-eye"></i> Ver Columnas'
        $(document).ready(function() {
                $('#tabla_agenda_responsable').DataTable(json_data_table_special);
        } );
    </script>
@endpush
