@php($titulo_reporte = "Tipos de proceso en tramite desde el año $array_rango_anio[0] al $array_rango_anio[1] en estatus ".$id_estados_proceso_fam_tipo_proceso->first())
@php($subtitulo = "Detalle de Procesos de familia en estatus ".$id_estados_proceso_fam_tipo_proceso->first())
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> 2.2 Tipos de procesos en trámite por año de inicio del proceso </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_familia_tipo_proceso" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="{{ $consulta_proceso_desglosado->count()+2 }}">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Tipo Proceso</th>
                                @foreach($rango_anios as $kepi => $vepi)
                                    <th>{{ $vepi }}</th>
                                @endforeach
                                <th>Sub-total</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($consulta_proceso_desglosado->sortBy("nombre_tipo_proceso")->groupBy("id_tipo_proceso") as $kap =>$vap)
                            <tr>
                                <td>{{ $data_x[]=$vap->first()->nombre_tipo_proceso }}</td>
                                @php($sub_total= [])
                                @foreach($rango_anios as $kepf => $vepf)
                                    @php($sub_consulta = $consulta_proceso_desglosado->where("anio",$vepf)->where("id_tipo_proceso",$kap))
                                    <td>{{ $sub_total[]= $sub_consulta->pluck("cantidad")->sum()  }}</td>
                                @endforeach
                                <td>{{ $data_y[]=array_sum($sub_total) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @php($sub_t_total= null)
                            @foreach($rango_anios as $kepf => $vepf)
                                @php($sub_consulta = $consulta_proceso_desglosado->where("anio",$kepf))
                                <td>{{ $sub_consulta->sum("cantidad" )  }}</td>
                            @endforeach
                            <td>{{ array_sum($data_y) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_tipo_proceso" class="chart">
                        Area g_por_tipo_proceso
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_tipo_proceso = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_tipo_proceso').highcharts(
                    {!! $js_data_chart_tipo_proceso !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_familia_tipo_proceso').DataTable(json_data_table);
        } );
    </script>
@endpush
