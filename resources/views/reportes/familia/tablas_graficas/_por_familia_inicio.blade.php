@php($titulo_reporte = "Procesos de familia hasta el año $array_rango_anio[1]")
@php($subtitulo = "Detalle de Procesos de familia")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b>2.1 Procesos de familia por año de inicio del proceso </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_familia_inicio" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="{{ $id_estados_proceso_fam_anio_des->count()+2 }}">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Año de Inicio</th>
                                @foreach($id_estados_proceso_fam_anio_des as $kepi => $vepi)
                                    <th>{{ $vepi }}</th>
                                @endforeach
                                <th>Sub-total</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($consulta_proceso->groupBy("anio") as $kap =>$vap)
                            <tr>
                                <td>{{ $data_x[]=$kap }}</td>
                                @php($sub_total= [])
                                @foreach($id_estados_proceso_fam_anio_des as $kepf => $vepf)
                                    @php($sub_consulta = $consulta_proceso->where("anio",$kap))
                                    <td>{{ $sub_total[]= $sub_consulta->where("id_estado",$kepf)->sum("cantidad")  }}</td>
                                @endforeach
                                <td>{{ $data_y[]=array_sum($sub_total) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @php($sub_t_total= null)
                            @foreach($id_estados_proceso_fam_anio_des as $kepf => $vepf)
                                @php($sub_consulta = $consulta_proceso->where("id_estado",$kepf))
                                <td>{{ $sub_consulta->sum("cantidad" )  }}</td>
                            @endforeach
                            <td>{{ array_sum($data_y) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_familia_inicio" class="chart">
                        Area g_por_familia_inicio
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_familia_inicio = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_familia_inicio').highcharts(
                    {!! $js_data_chart_familia_inicio !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_familia_inicio').DataTable(json_data_table);
        } );
    </script>
@endpush
