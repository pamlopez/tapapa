<style>
    .header_fijo {
        width: 750px;
        table-layout: fixed;
        border-collapse: collapse;
    }
    .header_fijo thead {
        background-color: #333;
        color: #FDFDFD;
    }
    .header_fijo thead tr {
        display: block;
        position: relative;
    }
    .header_fijo tbody {
        display: block;
        overflow: auto;
        width: 100%;
        height: 300px;
    }

     .tbl-encabezados{
        background: #ad1457;
        color: white;
    }
</style>