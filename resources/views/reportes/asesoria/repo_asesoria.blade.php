@extends('layouts.app')
@include('reportes._style')
@push('styles')
    <link rel="stylesheet" href="{{ url("/css/reportes/bootstrap-datetimepicker/bootstrap-datetimepicker.css") }}">
    <link rel="stylesheet" href="{{ url("/css/reportes/datatables/fixedColumns.dataTables.min.css") }}">
    <link rel="stylesheet" href="{{ url("/css/reportes/datatables/buttons.dataTables.css") }}">
@endpush
@push('javascript')
    <script src="{{ url('/js/reportes/data_tables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/buttons.flash.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/jszip.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/pdfmake.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/vfs_fonts.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/buttons.html5.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/buttons.print.min.js') }}"></script>
    <script type="text/javascript">
        var base64_data_table = '{!! $json_data_table !!}';
        var json_data_table = JSON.parse(atob(base64_data_table));
    </script>
@endpush
@section('content')

    <div class="content" style="margin-top: -3%;">
        <div class="clearfix"></div>
        @include('partials.control_fechas_anios')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_anio')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_mes')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_abogada')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_proceso')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_estatus_demanda')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_proyecto_mes')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_proyecto')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_demanda_curso')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_demanda_archivo')
        <div class="clearfix"></div>
        @include('reportes.asesoria.tablas_graficas._por_demanda_iniciada')
    </div>
@endsection
