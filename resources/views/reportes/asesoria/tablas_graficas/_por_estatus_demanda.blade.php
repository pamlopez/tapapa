@php($titulo_reporte = "Cálculo de estatus de procesos de demanda del  año  $array_rango_anio[1]")
@php($subtitulo = "Detalle estatus de procesos de demanda")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b>1.5 Status - {{ $titulo_reporte }}</b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $subtitulo }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_estatus" class="table table-hover table-bordered ">
                        <thead>
                        <tr class="tbl-encabezados">
                            <th colspan="2">
                                <h4 class="text-center">
                                    <b>{{ $subtitulo }}</b>
                                </h4>
                            </th>
                        </tr>
                        <tr class="tbl-encabezados">
                            <th>Estatus</th>
                            <th>Cantidad</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @foreach($estatus_proceos as $kam =>$vam)
                            @php($busca_cantidad=$consulta_proceso->where("id_estado",(int)$kam)->first())
                            <tr>
                                <td>{{ $data_x[]=$vam }}</td>
                                <td>{{ $total[]= isset($busca_cantidad->cantidad) ? $busca_cantidad->cantidad : 0 }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            <td>{{ array_sum($total) }}</td>
                            @php($data_y = $total)
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_estatus" class="chart">
                        Area g_estatus
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@php($js_data_chart_estatus = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_estatus').highcharts(
                    {!! $js_data_chart_estatus !!}
            )}
        );

        $(document).ready(function() {
            $('#tabla_estatus').DataTable(json_data_table);
        } );
    </script>
@endpush
