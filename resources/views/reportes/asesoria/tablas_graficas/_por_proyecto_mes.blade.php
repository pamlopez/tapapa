@php($titulo_reporte = "Proyectos de demanda según año  $array_rango_anio[1]")
@php($subtitulo1 = "Detalle de proyectos de demanda por mes")
@php($subtitulo2 = "Proyectos de demanda por proyecto")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> 1.6.1 {{ $subtitulo1  }} </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_proyecto_mes" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="2">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo1 }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Mes</th>
                                <th>Sub-Total Proyectos</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @php($total_iniciado=null)
                        @php($total_piniciar=null)
                        @foreach($array_meses as $kam =>$vam)
                            <tr>
                                <td>{{ $data_x[]=$mes[$kam] }}</td>
                                @php($consulta = $proyectos->where("mes",$vam))
                                <td>{{ $total[]=$consulta->sum("cantidad")  }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            <td>{{ array_sum($total) }}</td>
                            @php($data_y=$total)
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_proyecto_mes" class="chart">
                        Area g_proceso
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_p_mes = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_proyecto_mes').highcharts(
                    {!! $js_data_chart_p_mes !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_proyecto_mes').DataTable(json_data_table);
        } );
    </script>
@endpush
