@php($titulo_reporte = "Demandas en curso año  $array_rango_anio[1]")
@php($subtitulo = "Detalle de demandas en Curso")
@php($data_pie["data"] = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> <i class="fa fa-file-excel-o" aria-hidden="true"></i> Procesos iniciados y por iniciar </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_demanda_curso" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="2">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Tipo de Demanda</th>
                                <th>Sub-Total en curso</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @foreach($obj_cruso as $kap =>$vap)
                            @php($data_pie["data"][] = ["name"=>$vap->descripcion, "y"=>$vap->cantidad])
                            <tr>
                                <td>{{ $vap->descripcion }}</td>
                                <td>{{ $vap->cantidad  }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            <td>{{ $obj_cruso->sum() }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_demanda_curso" class="chart">
                        Area g_por_demanda_curso_arch
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_curso =\App\Models\asesoria::g_pie($data_pie,$titulo_reporte,$subtitulo,$subtitulo))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_demanda_curso').highcharts(
                    {!! $js_data_chart_curso !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_demanda_curso').DataTable(json_data_table);
        } );
    </script>
@endpush
