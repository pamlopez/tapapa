@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-danger">
            <div class="box-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6" align="left">
                            <h2> <b> Metodología </b></h2>

                           <h3>
                               1. Selección de proyectos en base a criterios preestablecidos. <br><br>
                               2. Notificación escrita a entidades o unidades ejecutoras para informarles de los proyectos seleccionados y procedimiento a aplicar.<br><br>
                           </h3>

                        </div>
                        <div class="col-md-6">
                            <img src="{{asset('images/meto1.png') }}" alt="">


                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6" align="left">
                            <h3>
                                3. Revisión de divulgación proactiva y preparación de la visita de inspección en campo.<br><br>
                                4. Visita de inspección en campo.<br><br>
                                5. Informe de visita de inspección en campo.<br><br>
                                6. Revisión de divulgación reactiva.<br><br>
                            </h3>

                        </div>
                        <div class="col-md-6">
                            <img src="{{asset('images/meto2.png') }}" alt="">


                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6" align="left">
                            <h3>
                                7. Elaboración de informe final del proyecto.<br><br>
                                8. Elaboración del informe de aseguramiento (incluye varios proyectos y un análisis estadístico de los niveles de divulgación y cumplimiento de condiciones contractuales).<br><br>
                                9. Presentación pública de los proyectos analizados.<br><br>
                            </h3>

                        </div>
                        <div class="col-md-6">
                            <img src="{{asset('images/meto3.png') }}" alt="">


                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@push('javascript')
<script>



</script>
@endpush

