@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <section class="content-header">
            <h1>
                <i class="fa fa-comments" aria-hidden="true"></i> Contáctenos
            </h1>
        </section>
        <div class="content">
            @include('adminlte-templates::common.errors')
            <div class="box box-primary">

                <div class="box-body">
                    <div class="row">
                        {!! Form::open(['route' => 'costContactenos.store']) !!}

                            @include('cost_contactenos.fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>    
    </div>
    
@endsection
