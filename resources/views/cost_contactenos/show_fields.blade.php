<!-- Id Cost Contactenos Field -->
<div class="form-group">
    {!! Form::label('id_cost_contactenos', 'Id Cost Contactenos:') !!}
    <p>{!! $costContactenos->id_cost_contactenos !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $costContactenos->email !!}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefono:') !!}
    <p>{!! $costContactenos->telefono !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $costContactenos->descripcion !!}</p>
</div>

