@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <h1>
            Cost Contactenos
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_contactenos.show_fields')
                    <a href="{!! route('costContactenos.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
