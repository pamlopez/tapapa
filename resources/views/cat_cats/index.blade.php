@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <section class="content-header">
                <h1 class="pull-left">
                    Gestión de c&aacute;talogos
                </h1>
                <h1 class="pull-right">
                   <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('catCats.create') !!}">
                       <i class="fa fa-plus" aria-hidden="true"></i>  Agregar c&aacute;talogo
                   </a>
                </h1>
            </section>
            <div class="content">
                <div class="clearfix"></div>

                @include('flash::message')

                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">
                            @include('cat_cats.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

