@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <a href="{!! route('catCats.index') !!}" class="btn btn-default">Regresar</a>
            Detalle de catalogos
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cat_cats.show_fields')
                </div>
            </div>
        </div>
    </div>
@endsection
