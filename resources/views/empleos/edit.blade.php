@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Empleo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($empleo, ['route' => ['empleos.update', $empleo->id], 'method' => 'patch']) !!}

                        @include('empleos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection