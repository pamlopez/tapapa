@extends('layouts.app')
@section('content')
    <div class="row">
        <a href="{!!url('administracion')!!}" class="btn btn-info col-sm-offset-1"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Volver</a>
    </div>
    <h3 style="text-align:center">
        <b>Asignación de accesos al sistema</b>
    </h3>
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-user" aria-hidden="true"></i> Datos del usuario
                </h3>
            </div>
            <div class="panel-body">
                {!! Form::model($user, ['url' => ['edit_user', $user->id], 'method' => 'post']) !!}
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Nombre del usuario:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        {!! Form::label('email', 'Correo electr&oacute;nico:') !!}
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                    {!! Form::hidden('id',$user->id, ['class' => 'form-control']) !!}

                    <table class="table table-bordered">
                        <tr>
                            <th>Fecha de creaci&oacute;n:</th>
                            <td>{{\Carbon\Carbon::parse($user->created_at)->format('d/m/Y')}}</td>
                        </tr>
                    </table>

                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-cogs" aria-hidden="true"></i> Elegir los accesos al sistema para este usuario</h3>
                        </div>
                        <div class="panel-body">
                            @include('auth.administracion._form')
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <div class="form-group row">
                    <div align="center">
                        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('administracion.index') !!}" class="btn btn-default">Cancelar</a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection