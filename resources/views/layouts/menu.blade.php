<li class="dropdown">
    <a href="{!! url('costProyectos?b=1') !!}">
        <i class="fa fa-fw fa-search-plus" aria-hidden="true" style="color: #bf360c"></i>
        <span>Búsqueda de Proyectos</span></a>
</li>
<li class="dropdown"><a href="{!! url('costMetodologia') !!}"><i class="fa fa-fw fa-building" aria-hidden="true"
                                                                 style="color: #00c853"></i>
        <span>Metodología</span></a>
</li>
<li class="dropdown">
    <a href="{!! url('indicadores') !!}"><i class="fa fa-fw fa-bar-chart" aria-hidden="true" style="color: #e65100"></i>
        <span>Dashboard</span></a>
</li>

<!--
<li class="header">GESTIÓN DE INFORMACIÓN</li>

<li><a href="{!! url('atencionInicials') !!}"><i class="fa fa-id-card-o" aria-hidden="true"></i> <span>Atencion inicial</span></a></li>
<li><a href="{!! url('menuLegal') !!}"><i class="fa fa-balance-scale" aria-hidden="true"></i> <span>Área legal</span></a></li>
<li><a href="{!! url('agendas') !!}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda legal</span></a></li>
<li><a href="{{  url('agendaPsicologia')}}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda psicología</span></a></li>
<li><a href="{!! url('agendaTrabajoSocial') !!}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda trabajo social</span></a></li>
<li><a href="{!! url('agendaAlbergue') !!}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda albergue</span></a></li>


<li class="header">CONTROL DE ASISTENCIA</li>
<li><a href="{!! url('controlAsistencias') !!}"><i class="fa fa-list-alt" aria-hidden="true"></i> <span>Control de asistencia</span></a></li>


<li class="header">REPORTES</li>
<li><a href="{!! url('reportes/repoMingob') !!}">  <i class="fa fa-file-excel-o" aria-hidden="true"></i><span>Reporte MINGOB</span></a></li>
<li><a href="{!! url('reportes/analisis') !!}">  <span class="glyphicon glyphicon-book" aria-hidden="true"></span><span>Informe estadístico de <br> acciones área Legal</span></a></li>
<li><a href="{!! url('reportes/indexAgendaLegal') !!}">  <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span><span>Informe estadístico de acciones  <br> Agenda Legal</span></a></li>
-->
@if(Auth::check())
    @if(\App\User::tiene_rol(1)==true)
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-list-alt" aria-hidden="true" style="color: #1633c8"></i> Catalogos <span
                        class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{!! url('participantes') !!}"> <i class="fa fa-users" aria-hidden="true"
                                                               style="color: #c81823"></i> Gestión de
                        Participantes</a></li>
                <li class="divider"></li>

                <li><a href="{!! url('catCats') !!}"><i class="fa fa-cube" aria-hidden="true"></i> Gestión de Catalogos</a>
                </li>
                <li><a href="{!! url('catItems') !!}"><i class="fa fa-cubes" aria-hidden="true"
                                                         style="color: #827717"></i>
                        <span>Gesti&oacute;n de items de cat&aacute;logos</span></a></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                        class="fa fa-address-card" aria-hidden="true"
                        style="color: #134d7a"></i> Usuarios <span
                        class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/administracion') }}"><i class="fa fa-user-plus" aria-hidden="true"
                                                              style="color: #039be5"></i>
                        <span>Administración usuario</span></a>
                </li>
                <li class="divider"></li>
            </ul>
        </li>

    @endif
@endif
@push('javascript')

    <script>
        jQuery(document).ready(function ($) {
            try {
                var window_loc = window.location.href.split('?')[0], target = $('.sidebar-menu li a'),
                    exprecion = /\d+/;
                var url_valida = exprecion.test(window_loc) && window_loc.match(exprecion).length > 0 ? window_loc.replace(/[0-9]/g, '').slice(0, -1) : window_loc
                url_valida = url_valida.replace('//edi', '')
                url_valida = url_valida.replace('/create', '')
                target.each(function (v, k) {
                    var element = $(this), url_href = element.attr("href");
                    if (url_href == url_valida) {
                        element.closest("li").addClass('active');
                    }
                })
            } catch (e) {
                console.log(e.message)
            }
        });

    </script>

@endpush