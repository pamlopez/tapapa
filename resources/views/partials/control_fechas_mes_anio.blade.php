<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
           <b>Personalizar la información visualizada</b>
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa {{isset($collapse)?'fa-minus':' fa-plus'}}"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        {{ Form::open(array('method' => 'get',"id"=>"form_anio")) }}
            <div class="row">
                <div class="col-md-4 ">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Rango de fechas: </label>
                    <div class="input-daterange input-group" id="datepicker">
                        <span class="input-group-addon">Del</span>
                        <input type="text" class="input-sm form-control" name="fh_inicio" placeholder="Fecha de Inicio" value="{{ $rango_fechas[0] }}" required autocomplete="off" />
                        <span class="input-group-addon">Al</span>
                        <input type="text" class="input-sm form-control" name="fh_final" placeholder="Fecha de Fin" value="{{ $rango_fechas[1] }}" required autocomplete="off" />
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('', '&nbsp;') !!}
                        <button type="submit" autofocus class="btn bg-teal form-control" ><i class="fa fa-check" aria-hidden="true"></i>  Buscar</button>
                    </div>

                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('', '&nbsp;') !!}
                        <a type="button" class="btn bg-navy form-control" href="{{ $request->url() }}"><i class="fa fa-refresh" aria-hidden="true"></i>  Reiniciar</a>
                    </div>

                </div>
            </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>



@push("head")
{{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
<head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush
@push('styles')
    <link rel="stylesheet" href="{{ url("/css/reportes/bootstrap-datepicker/bootstrap-datepicker3.min.css") }}">
@endpush


@push('javascript')
    <script src="{{ url('/js/reportes/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script>

        $(function(){
            var date=new Date();
            var year=date.getFullYear();
            var month=date.getMonth();

            $('.input-daterange').datepicker({
                format: "mm-yyyy",
                viewMode: "months",
                minViewMode: "months",
                startDate: new Date(2011, 12, '31'),
                endDate: new Date(year, month, '01'),
                autoclose: true
            });
        });


    </script>

@endpush