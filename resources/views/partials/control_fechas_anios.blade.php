<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
           <b>Personalizar la información visualizada</b>
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa {{isset($collapse)?'fa-minus':' fa-plus'}}"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        {{ Form::open(array('method' => 'get',"id"=>"form_anio")) }}
            <div class="row">
                <div class="col-md-2 ">
                    <div class="form-group">
                        <label><i class="fa fa-calendar" aria-hidden="true"></i> Año: </label>
                        {!! Form::text('fh_entrevista_legal', null, ['class' => 'form-control',"id"=>"anio",'data-date'=>"12-02-2012","readonly"=>"readonly",'value'=>$request->fh_entrevista_legal]) !!}
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('', '&nbsp;') !!}
                        <button type="submit" class="btn bg-teal form-control" ><i class="fa fa-check" aria-hidden="true"></i>  Buscar</button>
                    </div>

                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('', '&nbsp;') !!}
                        <a type="button" class="btn bg-navy form-control" href="{{ $request->url() }}"><i class="fa fa-refresh" aria-hidden="true"></i>  Reiniciar</a>
                    </div>

                </div>
            </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>



@push("head")
{{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
<head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush



@push('javascript')
    <script src="{{ url('/js/reportes/bootstrap-datetimepicker/moment.js') }}"></script>
    <script src="{{ url('/js/reportes/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        $(document).ready(function() {

            $('#anio').datetimepicker({
                format      :   "YYYY",
                viewMode    :   "years",
                minDate: new Date({{ $anio_inicio }}, 1 - 1, 1),
                maxDate: new Date((new Date).getFullYear(), 1, 1),
                ignoreReadonly: true
            })
            $('#anio').val({{ $request->fh_entrevista_legal }});
        } );

    </script>

@endpush