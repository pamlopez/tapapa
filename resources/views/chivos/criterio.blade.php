{{-- Control tipo dropdouwn para criterio fijo --}}
@php($chivo_tamanio = isset($chivo_tamanio)?$chivo_tamanio:'col-sm-6')

{{-- Criterio fijo --}}
<div class="form-group {{$chivo_tamanio}}">
    {!! Form::label($chivo_control, $chivo_texto) !!}
    {!! Form::select($chivo_control, \App\Models\criterio_fijo::listado_items($chivo_id_grupo,'Seleccionar'), $chivo_default,['class' => 'form-control']) !!}
</div>

@push('javascript')
    <script>
        var control ='#<?=($chivo_control);?>';
        $(control).select2({
            placeholder: 'Select an option'
        });
    </script>
@endpush