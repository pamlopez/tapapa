@extends('layouts.app')
@push("head")
{{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
<head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush


@section('content')
    <div class="clearfix"></div>
    <h1>Controles listos para ser utilizados</h1>


    {{-- Fecha y Hora --}}
     <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Selectores de fecha/hora
                </h3>

            </div>
            <div class="box-body">
                <div class="col-sm-6">
                    <h5>Fecha</h5>
                        @include('chivos.fecha', ['chivo_control' => 'fecha'
                                        , 'chivo_default'=>'2017-09-20'
                                        ,'chivo_texto'=>'Fecha de nacimiento'])
                </div>
                <div class="col-sm-6">
                    <h5>Hora</h5>
                    @include('chivos.hora', ['chivo_control' => 'hora'
                                    , 'chivo_default'=>''
                                    ,'chivo_texto'=>'Hora '])
                </div>
                <b>Atención:</b> El campo envia un campo oculto con el sufijo _submit en formato yyyy-mm-dd

            </div>
        </div>
     </div>


    {{-- CATALOGOS --}}

    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Elegir opción de un catálogo
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-6">
                    <h5>Pre seleccionado</h5>
                    @include('chivos.catalogo', ['chivo_control' => 'id_color'
                                            ,'chivo_id_cat'=>1
                                            , 'chivo_default'=>2
                                            ,'chivo_texto'=>'¿cual es su color favorito?'])
                </div>
                <div class="col-sm-6">
                    <h5>Sin preseleccionar: Utiliza el predeterminado según la BD</h5>
                    @include('chivos.catalogo', ['chivo_control' => 'id_color2'
                                            ,'chivo_id_cat'=>1
                                            , 'chivo_default'=>null
                                            ,'chivo_texto'=>'Elija color'])
                </div>
            </div>
        </div>
    </div>

    {{-- Criterio Fijo --}}

    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Elegir opción de un criterio fijo
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-6">
                    <h5>Pre seleccionado</h5>
                    @include('chivos.criterio', ['chivo_control' => 'id_criterio'
                                            ,'chivo_id_grupo'=>1
                                            , 'chivo_default'=>1
                                            ,'chivo_texto'=>'¿es usted distraido?'])
                </div>
                <div class="col-sm-6">
                    <h5>Sin preseleccionar: Utiliza el predeterminado según la BD</h5>
                    @include('chivos.criterio', ['chivo_control' => 'id_criterio2'
                                            ,'chivo_id_grupo'=>1
                                            , 'chivo_default'=>null
                                            ,'chivo_texto'=>'¿es usted distraido?'])
                </div>
            </div>
        </div>
    </div>

    {{-- Departamento/Municipio --}}

    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Elegir departamento/municipio
                </h3>
            </div>
            <div class="box-body">
                <h5>Pre seleccionado</h5>
                @include('chivos.geo2', ['chivo_control' => 'id_muni'
                                               , 'chivo_default'=>41])
                <h5>Sin pre seleccionar</h5>
                @include('chivos.geo2', ['chivo_control' => 'id_muni2'
                                               , 'chivo_default'=>null])
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Elegir departamento/municipio/lugar poblado
                </h3>
            </div>
            <div class="box-body">
                <h5>Pre seleccionado</h5>
                @include('chivos.geo3', ['chivo_control' => 'id_lp'
                                               , 'chivo_default'=>90668])

                <h5>Sin pre seleccionar</h5>
                @include('chivos.geo3', ['chivo_control' => 'id_lp2'
                                               , 'chivo_default'=>null])
            </div>
        </div>
    </div>

    {{-- Fiscalías MP --}}
    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Elegir fiscalía (autofill)
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-6">
                    <h5>Sin pre seleccionar</h5>
                    @include('chivos.mp', ['chivo_control' => 'id_mp'
                                                   , 'chivo_default'=>null])
                </div>
                <div class="col-sm-6">
                    <h5>Preseleccionado: 19</h5>
                    @include('chivos.mp', ['chivo_control' => 'id_mp2'
                                                   , 'chivo_default'=>58])
                </div>
            </div>
        </div>
    </div>



    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Elegir Juzgado
                </h3>
            </div>
            <div class="box-body">

                <h5>Pre seleccionado</h5>
                @include('chivos.oj', ['chivo_control' => 'id_juzgado'
                                               , 'chivo_default'=>13])
                <h5>Sin pre seleccionar</h5>
                @include('chivos.oj', ['chivo_control' => 'id_juzgado2'
                                               , 'chivo_default'=>null])
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Elegir sede PNC
                </h3>
            </div>
            <div class="box-body">
                <h5>Pre seleccionado</h5>
                @include('chivos.sedes', ['chivo_control' => 'id_sede'
                                               , 'chivo_default'=>59])

                <h5>Sin pre seleccionar</h5>
                @include('chivos.sedes', ['chivo_control' => 'id_sede2'
                                               , 'chivo_default'=>null])

                <h5>Con opcion de "Sin especificar"</h5>
                @include('chivos.sedes', ['chivo_control' => 'id_sede3'
                                               , 'chivo_vacio'=>'(Sin especificar)'
                                               , 'chivo_default'=>null])
                <h5>Con opcion de "Sin especificar" y valor pre seleccionado</h5>
                @include('chivos.sedes', ['chivo_control' => 'id_sede4'
                                               , 'chivo_vacio'=>'SIN ESPECIFICAR'
                                               , 'chivo_default'=>59])
            </div>
        </div>
    </div>

    {{-- SISPE --}}
    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Integracion a SISPE
                </h3>

            </div>
            <div class="box-body">
                <div class="col-sm-6">
                    <h5>En blanco</h5>
                    @include('chivos.sispe', ['chivo_control' => 'nip'
                                    , 'chivo_default'=>''
                                    ,'chivo_texto'=>'NIP'])
                </div>
                <div class="col-sm-6">
                    <h5>Predeterminado</h5>
                    @include('chivos.sispe', ['chivo_control' => 'nip2'
                                    , 'chivo_default'=>'21826'
                                    ,'chivo_texto'=>'NIP'])
                </div>

                <b>Atención:</b> Usar este codigo como base para almacenar localmente los datos personales

            </div>
        </div>
    </div>

    {{-- Fiscalías MP --}}
    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Grafiquita
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-6">

                    @include('chivos.grafica')
                </div>

            </div>
        </div>
    </div>






    <div class="clearfix"></div>
@endsection

{{-- Para que los AJAX no den problemas, este script utiliza el header definido al principio --}}
@push('javascript')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@endpush