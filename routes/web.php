<?php
setlocale(LC_TIME, 'es_GT.UTF-8','es_GT.ISO-8859-1','es_GT','es_ES','es_ES','spanish');
//Carbon
use Carbon\Carbon;
Carbon::setLocale('es');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();
Route::get('/', function () {
    return redirect('costProyectos');
});

//Muestra de chivos de controles listos para ser usados
Route::get('chivos', function () {
    return view('chivos.demo');
});

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('catCats', 'cat_catController');
    Route::resource('catItems', 'cat_itemController');
    Route::get('json/catalogo','cat_itemController@json');
    Route::get('itemdeshabilitado/{id}','cat_itemController@itemdeshabilitado');

    //cost loqueado

    //Controles geograficos
    Route::get('json/geo','geoController@mostrar_hijos');
    Route::resource('administracion','AdministracionController');
    Route::resource('sedes', 'sedeController');
    Route::post('edit_user/{id}','AdministracionController@edit_user');
    Route::get('reiniciar_password/{id}','AdministracionController@reiniciar_password');
    Route::get('reiniciar_password_perfil/{id}','AdministracionController@reiniciar_password_perfil');
    Route::get('password_perfil_change/{id}','AdministracionController@password_perfil_change');
    Route::post('password_change','AdministracionController@password_change');
    Route::get('password_deshabilitar/{id}','AdministracionController@password_deshabilitar');
    Route::get('password_habilitar/{id}','AdministracionController@password_habilitar');


    //upload de fotografias del proyecto
    Route::get('image_upload','ImageController@imageUpload');
    Route::post('image_upload_save','ImageController@imageUploadPost');
    Route::post('eliminar_foto/{id}','ImageController@eliminar_foto');

});

//Controles geograficos
Route::post('json/geo','geoController@mostrar_hijos');
Route::post('json/geo_todo','geoController@mostrar_hijos_con_todo');

//Pruebas
//Envio de correos
Route::get('/test/correo', 'emailController@test');

Route::resource('empleos', 'empleoController');

Route::get('identificacionProyecto/{id}', 'cost_proyectoController@detalle_plano_ident_proyecto');
Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController');
Route::get('costProyectoDisPlans_plano', 'cost_proyecto_dis_planController@vista_plana');


Route::resource('costProyectos', 'cost_proyectoController');

Route::get('costProyectos/proyecto/{id}','cost_proyectoController@ocds');

Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController');

Route::resource('costProyectoPlanificas', 'cost_proyecto_planificaController');

Route::resource('costProyectoSupervisions', 'cost_proyecto_supervisionController');

Route::resource('costMetodologia','cost_metodologiaController');

Route::get("indicadores","indicadoresController@indicadores");

Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController');

Route::resource('costAmpliacions', 'cost_ampliacionController');

Route::resource('costActas', 'cost_actaController');

Route::resource('costAcuerdoFinanciamientos', 'cost_acuerdo_financiamientoController');

Route::resource('costFianzas', 'cost_fianzaController');

Route::resource('costInfoContratos', 'cost_info_contratoController');

Route::resource('costPagoEfectuados', 'cost_pago_efectuadoController');

Route::resource('descargaResponsables', 'descarga_responsableController');

Route::resource('costContactenos', 'cost_contactenosController');

Route::resource('costLiquidacions', 'cost_liquidacionController');

Route::resource('costLiquidacionAlcances', 'cost_liquidacion_alcanceController');

Route::resource('costLiquidacionMontos', 'cost_liquidacion_montoController');

Route::resource('costLiquidacionTiempos', 'cost_liquidacion_tiempoController');



Route::resource('costEstimacions', 'cost_estimacionController');

Route::resource('participantes', 'participanteController');

Route::resource('costOferentes', 'cost_oferenteController');

Route::resource('costOperadors', 'cost_operadorController');






Route::resource('documentos', 'documentoController');